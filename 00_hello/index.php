<?php

//Простой вывод текста
echo "Hello World";

//Однострочный комментарий (интерпретаор игнорирует все что идет после // до конца строки)

/**
 * Много строчный комментарий
 * Все что написанно в этом блоке будет проигнарированно
 * интерпретатором PHP. 
 * 
 * Ниже демонстрируется как комментируются функции и методы на PHP.
 * Комментировать методы очень полезно. В этом случае IDE будет подсказывать
 * (автодополнять ввод), а так же выводить описание функции. Кстати этот абзац
 * текста будет описанием функции.
 * @param string $name описание переменной
 * @param integer $age описание переменной 2
 * @return bool описание того что вернет функция
 */
