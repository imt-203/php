<?php
/**
 * Сгруппировать файлы с расширением *.bk таким образом:
 * 2018
 *     01
 *       01-01-2018.bk
 *       13-01-2018.bk
 *     02
 *       01-02-2018.bk
 * 2017
 *     02
 *       01-02-2017.bk
 *      
 */

function publish($filename, $year, $month, $day)
{
    $month = str_pad($month, 2, "0", STR_PAD_LEFT);
    $path = __DIR__."\\backup";
    $root = __DIR__;
    $info = pathinfo($root."\\".$filename);
    $newFileName = $day."-".$month."-".$year.".".$info['extension'];
    if(!is_dir($path)){
        mkdir($path);
    }
    $destDir = $path.'\\'.$year.'\\'.$month;
    if(!is_dir($destDir)){
        mkdir($destDir, 0777, true);
    }
    copy($root."\\".$filename, $destDir."\\".$newFileName);
}
$files = scandir(__DIR__);
foreach($files as $f){
    if(!preg_match('/\.bk$/', $f))
        continue;
    
    $result = preg_match('/(?<day>\d{2})[-_](?<month>\d{1,2})[-_](?<year>\d{4})/', $f, $mathes);
    if($result){
        publish($f, $mathes['year'], $mathes['month'], $mathes['day']);
    }
}