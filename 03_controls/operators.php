<?php
$x = 5;
$z = 6;

var_dump($x == $z);// Вернет false
var_dump($x != $z); //Вернет true

/**
 * Арифметические операторы
 */
var_dump($x + $z);  //Вернет 11
var_dump($z - $x);  //Вернет 1
var_dump($x > $z);  //Вернет false
var_dump($x ** 2);  //x в степени 2 (25)
var_dump($x / 2);   //результат деления х на 2 (2.5)
var_dump($x * 2);   //результат умножения на 2 (10)
var_dump($x++);     //Сперва выведет, а затем увеличит на еденицу (вернет 5)
var_dump(++$x);     //Сперва увеличит на еденицу, а затем вернет результат (вернет 7)
var_dump($z--);     //Сперва выведет, а затем уменьшит на еденицу (вернет 6)
var_dump(--$z);     //Сперва выведет, а затем уменьшит на еденицу (вернет 4)

$n = 10;
$n += 5;            //Присвоит или увеличит значение на 5. Вернет 15

$n = 10;
$n -= 5;            //Присвоит или уменьшит значение на 5. Вернет 5.

$a = ($b = 4) + 5;
echo $a;            //Вернет 9

$number = 5;
$line = '6';
var_dump($number == $line); //Вернет false


/**
 * Операторы присваивания
 */
$name = 'Oleg';
$greeting = 'Hello';
$greeting .= ", $name";
echo $greeting;     //Выведет Hello, Oleg


